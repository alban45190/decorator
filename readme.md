# Desing pattern

## Decorator

Le design pattern Decorateur permet d'ajouter dynamiquement des fonctionnalités à un objet. Il permet d'étendre les fonctionnalités d'un objet sans avoir à créer une nouvelle classe.