package fr.albanw.pattern;

import fr.albanw.pattern.cofee.Cofee;
import fr.albanw.pattern.cofee.Moka;
import fr.albanw.pattern.decorator.AbstractDecorator;
import fr.albanw.pattern.decorator.Chantilly;

public class Main {

    public static void main(String[] args) {
        Cofee cofee = new Moka();

        System.out.println(cofee.getDescription() + " (Price: " + cofee.getPrice() + "€)");

        AbstractDecorator decoratedCofee = new Chantilly(cofee);
        System.out.println(decoratedCofee.getDescription() + " (Price: " + decoratedCofee.getPrice() + "€)");

    }

}