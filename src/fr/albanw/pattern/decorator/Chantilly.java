package fr.albanw.pattern.decorator;

import fr.albanw.pattern.cofee.Cofee;

public class Chantilly extends AbstractDecorator {


    public Chantilly(Cofee cofee) {
        super(cofee);
    }

    @Override
    public String getDescription() {
        StringBuilder sb = new StringBuilder(super.getDescription());
        return sb.append(" with chantilly").toString();
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 0.5;
    }
}
