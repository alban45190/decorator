package fr.albanw.pattern.decorator;

import fr.albanw.pattern.cofee.Cofee;

public abstract class AbstractDecorator implements Cofee  {

    private final Cofee cofee;

    public AbstractDecorator(Cofee cofee) {
        this.cofee = cofee;
    }

    @Override
    public String getDescription() {
        return cofee.getDescription();
    }

    @Override
    public double getPrice() {
        return cofee.getPrice();
    }
}
