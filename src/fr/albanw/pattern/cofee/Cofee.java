package fr.albanw.pattern.cofee;

public interface Cofee {

    String getDescription();
    double getPrice();

}
