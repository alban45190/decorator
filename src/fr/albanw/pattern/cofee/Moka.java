package fr.albanw.pattern.cofee;

public class Moka implements Cofee {

    @Override
    public String getDescription() {
        return "Moka is a type of coffee";
    }

    @Override
    public double getPrice() {
        return 2.5;
    }
}
